package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];
	private static String MENSAJE = "Acierta la palabra";
	private static String RUTA = "src\\palabras.txt";
	private static String LINEA_DIFERENTE = "####################################";

	public static void main(String[] args) {
		
		String palabraSecreta;

		leerFichero(RUTA);

		Scanner input = new Scanner(System.in);

		palabraSecreta = conseguirPalabraSecreta();

		char[][] caracteresPalabra = new char[2][];
		rellenarMatriz(palabraSecreta, caracteresPalabra);

		String caracteresElegidos = "";
		System.out.println(MENSAJE);
		
		caracteresElegidos = buclePrograma(palabraSecreta, input, caracteresPalabra, caracteresElegidos);

		input.close();
	}

	private static String buclePrograma(String palabraSecreta, Scanner input, char[][] caracteresPalabra,
			String caracteresElegidos) {
		int fallos;
		boolean acertado;
		do {

			System.out.println(LINEA_DIFERENTE);

			comprobarPalabraSecreta(caracteresPalabra);

			caracteresElegidos = elegirLetraPalabra(input, caracteresElegidos);
			fallos = 0;

			fallos = buscaLetraPalabraElegida(caracteresPalabra, caracteresElegidos, fallos);

			pintarAhorcado(fallos);

			perdedor(palabraSecreta, fallos);
			acertado = true;
			acertado = compruebaGanador(caracteresPalabra, acertado);
			ganador(acertado);

		} while (!acertado && fallos < FALLOS);
		return caracteresElegidos;
	}

	private static boolean compruebaGanador(char[][] caracteresPalabra, boolean acertado) {
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		return acertado;
	}

	private static void ganador(boolean acertado) {
		if (acertado){
			System.out.println("Has Acertado ");
		}
	}

	private static void perdedor(String palabraSecreta, int fallos) {
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
	}

	private static int buscaLetraPalabraElegida(char[][] caracteresPalabra, String caracteresElegidos, int fallos) {
		boolean encontrado;
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
		return fallos;
	}

	private static String elegirLetraPalabra(Scanner input, String caracteresElegidos) {
		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();
		return caracteresElegidos;
	}

	private static void comprobarPalabraSecreta(char[][] caracteresPalabra) {
		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		//salto de linea en consola
		System.out.println();
	}

	private static void rellenarMatriz(String palabraSecreta, char[][] caracteresPalabra) {
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];
	}

	private static String conseguirPalabraSecreta() {
		return palabras[(int) (Math.random() * NUM_PALABRAS)];
	}

	private static void pintarAhorcado(int fallos) {
		switch (fallos) {
		case 1:

			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
	}

	private static void leerFichero(String ruta) {
		File fich = new File(ruta);
		Scanner inputFichero = null;

		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
	}

}