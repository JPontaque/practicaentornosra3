package refactorizacion;

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

// he borrado el "import java.*;" debido a que no estaba siendo utilizado,
// y para quitar Warnings.
// he cambiado este "import java.util.*;", ya que no era correcto.
import java.util.Scanner;

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */

// he cambiado el nombre de la clase, siempre debe empezar por mayuscula.
public class Refac {
	
	/* 
	 * he cambiado el nombre de esta variable, por algo que se entienda mejor
	 * y en Mayusculas.
	 */
	final static String MENSAJE = "Bienvenido al programa";
	//he anadido nuevas statics para evitar magic numbers por el programa
	final static int TAMANO = 7;
	final static int MULTIPLO = 5;
	final static int MULTIPLICAR = 3;
	final static int RESTAR = 1;
	final static int NUMERO_A = 7;
	final static int NUMERO_B = 16;
	final static int NUMERO_C = 25;

	public static void main(String[] args) {
		
		
		//hay que ponerle nombres coherentes al as variables
		//estas dos variables no se usan y crean warnings (lo quito abajo)
		String dni;
		String nombre;
		//hay que poner algo simbolico para llamar a un scanner
		Scanner sc = new Scanner(System.in);

		System.out.println(MENSAJE);
		System.out.println("Introduce tu dni");
		dni = sc.nextLine();
		System.out.println("Introduce tu nombre");
		nombre = sc.nextLine();
		
		//con esto quitamos los warnings de los Strings
		System.out.println("Tu DNI es: " + dni);
		System.out.println("Tu nombre es: " + nombre);
		
		//hay que ponerle un nombre mas claro a la variable
		//y declararla a la vez, y no en otras lineas
		//tambien evitamos mas magic numbers por el programa, para futuros problemas
		int numeroA = NUMERO_A;
		int numeroB = NUMERO_B;
		int numeroC = NUMERO_C;
		
		//vamos a evitar magin numbers poniendo final statics
		if (numeroA > numeroB || numeroC % MULTIPLO != 0 && 
				(numeroC * MULTIPLICAR - RESTAR) > numeroB / numeroC) {
			System.out.println("Se cumple la condición");
		}
		
		//poner parentesis para aclarar mas la operacion
		numeroC = (numeroA + numeroB) * (numeroC + (numeroB / numeroA));

		//es mejor darle una variable final static al array para su tamano
		//para evitar magic numbers
		String[] array = new String[TAMANO];
		array[0] = "Lunes";
		array[1] = "Martes";
		array[2] = "Miercoles";
		array[3] = "Jueves";
		array[4] = "Viernes";
		array[5] = "Sabado";
		array[6] = "Domingo";
		
		//evitar barras bajas y utilizar CamelUpperCase
		recorrerArray(array);
		
		//cerrar el scanner siempre
		sc.close();
	}

	//usar para escribir el CamelUpperCase siempre
	static void recorrerArray(String vectorDeStrings[]) {
		//la linea no tiene que pasar los 100 caracteres
		//evitar magic numbers
		//en un for la variable se llama 'i'
		for (int i = 0; i < vectorDeStrings.length; i++) {
			System.out.println("El dia de la semana en el que te encuentras [" + (i + 1) + "-7] "
					+ "es el dia: " + vectorDeStrings[i]);
		}
	}

}
