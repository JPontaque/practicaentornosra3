package refactorizacion;

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

//quitar imports que no se usan, y modificar los mal escritos
import java.util.Scanner;

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refac2 {

	static Scanner sc;
	static final int CANTIDAD_MAXIMA_ALUMNOS = 10;

	public static void main(String[] args) {
		
		//hay que ponerle un nombre mas simbolico al scanner, para que se entienda
		sc = new Scanner(System.in);
		
		//nombres mas coherentes
		int numero;
		//camelUpperCase
		int cantidadMaximaAlumnos;
		
		//agregar variables statics
		cantidadMaximaAlumnos = CANTIDAD_MAXIMA_ALUMNOS;
		//evitar magic numbers
		int[] arrays = new int[cantidadMaximaAlumnos];
		for (numero = 0; numero < cantidadMaximaAlumnos; numero++) {
			System.out.println("Introduce nota media de alumno");
			arrays[numero] = sc.nextInt();
		}
		
		//cambiar nombre
		System.out.println("El resultado es: " + recorrerArray(arrays));

		sc.close();
	}
	
	//cambiar nombre
	//evitar magic numbers
	static double recorrerArray(int vector[]) {
		double numeroC = 0;
		for (int i = 0; i < vector.length; i++) {
			numeroC = numeroC + vector[i];
		}
		return numeroC / vector.length;
	}

}
