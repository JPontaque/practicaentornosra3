package principal;

import java.util.Scanner;

public class Generador {

	public static void main(String[] args) {
		
		// 10 metodos usados.

		Scanner scanner = new Scanner(System.in);

		menuSwitch();

		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = opcionSwitch(scanner);
		System.out.println("Elige tipo de password: ");
		int opcion = opcionSwitch(scanner);

		String password = "";

		password = menuSwitch(longitud, opcion, password);
		
		mostrarPassword(password);
		scanner.close();
	}

	private static String menuSwitch(int longitud, int opcion, String password) {
		switch (opcion) {
		case 1:
			password = blucleInfinito(password, longitud, opcion);
			break;
		case 2:
			password = blucleInfinito(password, longitud, opcion);
			break;
		case 3:
			password = blucleInfinito(password, longitud, opcion);
			break;
		case 4:
			password = blucleInfinito(password, longitud, opcion);
			break;
		}
		return password;
	}

	private static String blucleInfinito(String password, int longitud, int opcion) {

		for (int i = 0; i < longitud; i++) {
			int numeroAleatorio = comprobarNumeroA(opcion);
			password = comprobarOpcion(password, longitud, opcion, numeroAleatorio);
		}

		return password;

	}

	private static int comprobarNumeroA(int opcion) {
		int numeroA;
		if (opcion == 4) {
			numeroA = (int) (Math.random() * 3);
			return numeroA;
		} else if (opcion == 3) {
			numeroA = (int) (Math.random() * 2);
			return numeroA;
		} else {
			return 0;
		}
	}

	private static String comprobarOpcion(String password, int longitud, int opcion, int numeroAleatorio) {

		if (opcion == 1) {

			password += letrasAleatorias();
			return password;

		} else if (opcion == 2) {

			password += numerosAleatorios();
			return password;

		} else if (opcion == 3) {

			if (numeroAleatorio == 1) {
				password += letrasAleatorias();
			} else {
				password += caracterAleatorio();
			}

			return password;

		} else {

			if (numeroAleatorio == 1) {
				password += letrasAleatorias();
			} else if (numeroAleatorio == 2) {
				password += caracterAleatorio();
			} else {
				password += numerosAleatorios();
			}

			return password;

		}

	}

	private static char caracterAleatorio() {
		return (char) ((Math.random() * 15) + 33);
	}

	private static int numerosAleatorios() {
		return (int) (Math.random() * 10);
	}

	private static char letrasAleatorias() {
		return (char) ((Math.random() * 26) + 65);
	}

	private static void mostrarPassword(String password) {
		System.out.println(password);
	}

	private static int opcionSwitch(Scanner scanner) {
		return scanner.nextInt();
	}

	private static void menuSwitch() {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}

}
